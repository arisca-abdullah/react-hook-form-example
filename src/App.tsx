import React from "react";
import { useForm } from 'react-hook-form';
import "./App.css";
import PasswordPopover from './PasswordPopover';

type FormInputs = {
  fullName: string,
  email: string,
  password: string,
  confirmPassword: string,
  agree: boolean,
  subscribe: boolean
}

function later(delay: number) {
  return new Promise((resolve) => {
    setTimeout(resolve, delay);
  });
}

function Form() {
  const { register, handleSubmit, errors, getValues, setError, clearErrors, formState, reset  } = useForm<FormInputs>();
  const onSubmit = async (data: FormInputs) => {
    await later(3000);
    console.log(data);
    reset();
  }

  return (
    <div>
      <h1> Create an Account </h1>
      <form>
        <label> Fullname * </label>
        <input type="text" name="fullName" ref={register({ required: 'Fullname is required' })} />
        {errors?.fullName && <p className="error">{errors.fullName.message}</p>}

        <label> Email *</label>
        <input
          type="email"
          name="email"
          ref={register({
            required: 'Email is required',
            pattern: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
          })}
        />
        {errors?.email  && errors?.email?.type === 'required' && <p className="error">{errors.email.message}</p>}
        {errors?.email  && errors?.email?.type === 'pattern' && <p className="error">Invalid Email</p>}

        <label htmlFor=""> Password * </label>
        <PasswordPopover>
          {(props) => (
            <input
              type="password"
              name="password"
              autoComplete="new-password"
              ref={register({ required: 'Password is required' })}
              onFocus={() => props.visible(true)}
              onBlur={() => props.visible(false)}
              onChange={() => props.validate('password', getValues, setError, clearErrors)}
            />
          )}
        </PasswordPopover>
        {errors?.password && <p className="error">{errors.password.message}</p>}

        <label htmlFor=""> Confirm Password * </label>
        <PasswordPopover>
          {(props) => (
            <input
              type="password"
              name="confirmPassword"
              autoComplete="new-password"
              ref={register({ required: 'Confirm Password is required' })}
              onFocus={() => props.visible(true)}
              onBlur={() => props.visible(false)}
              onChange={() => props.validate('confirmPassword', getValues, setError, clearErrors)}
            />
          )}
        </PasswordPopover>
        {errors?.confirmPassword && <p className="error">{errors.confirmPassword.message}</p>}

        <label className="row">
          <input type="checkbox" name="agree" ref={register()} />
          <span>I Agree to Term of Services and Privacy Policy * </span>
        </label>

        <label className="row">
          <input type="checkbox" name="subscribe" ref={register()} />
          <span>Subscribe to Newsletter</span>
        </label>

        <button type="button" onClick={handleSubmit(onSubmit)} disabled={formState.isSubmitting }>Sign Up</button>
      </form>
    </div>
  );
}

function App() {
  return <Form />;
}

export default App;
